// deno-lint-ignore-file
import {
  Application,
  Context,
  isHttpError,
  Status,
} from "https://deno.land/x/oak/mod.ts";
import { config } from "./config/config.ts";
import router from "./routes/index.ts";

const app = new Application();

// port
const PORT = parseInt(config.PORT) | 3000;

app.addEventListener("listen", ({ hostname, port, secure }) => {
  console.log(
    `Listening on: ${secure ? "https://" : "http://"}${hostname ??
      "localhost"}:${port}`,
  );
});

app.use(router.routes());
app.use(router.allowedMethods());

// si on arrive dans ce middleware c'est qu'aucune route n'a été trouvée plus haut
app.use(async (ctx: Context, next) => {
  ctx.response.status = 404;
  ctx.response.type = "text/html; charset=utf-8";
  ctx.response.body = `<H1>404</H1>`;
});
// cron
import "./cron/subscribe.ts";
// lancement serveur
// deno run --allow-net --allow-read --allow-write --allow-env --allow-plugin --unstable server.ts
await app.listen({ port: PORT });
