import { roleTypes } from "../types/rolesTypes.ts";
import { sexeTypes } from "../types/sexeTypes.ts";
import { BillType } from "./billType.ts";

type ChildTypes = {
  firstname?: string;
  lastname?: string;
  email?: string;
  sexe?: sexeTypes;
  role?: roleTypes;
  password?: string;
  dateNaissance?: Date;
  createdAt?: Date;
  updateAt?: Date;
  token?: string;
  subscription?: number;
};
export type UserTypes = {
  firstname?: string;
  lastname?: string;
  email?: string;
  sexe?: sexeTypes;
  role?: roleTypes;
  password?: string;
  date_naissance?: Date;
  subscription?: number;
  childs?: ChildTypes[];
  bills?: BillType[];
  token?: string;
  cardId?: string | null;
  cvc?: number | null;
  subscribeCreatedAt?: Date | null;
  subscribeUpdateAt?: Date | null;
};
