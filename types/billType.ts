export type BillType = {
  date_payment?: Date;
  id_Stripe?: string;
  montant_ht?: string;
  montant_ttc?: string;
  source?: "Stripe";
  createdAt?: Date;
  updateAt?: Date;
};
