export default interface CardTypes {
  _id: string | null;

  cartNumber: string;
  month: string;
  year: string;
  defaultCard: boolean;
}
