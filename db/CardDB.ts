// deno-lint-ignore-file
import UserInterfaces from "../interfaces/userInterfaces.ts";
import { db } from "./db.ts";
import CardTypes from "../types/cardTypes.ts";
import ChildInterface from "../interfaces/ChildInterfaces.ts";
import { Bson } from "https://deno.land/x/bson/mod.ts";
import CardInterfaces from "../interfaces/cardInterfaces.ts";

export default class CardDB {
  protected cardDB;
  constructor() {
    this.cardDB = db.collection<CardInterfaces>("cards");
  }

  async findByCardIdCard(idCard: string): Promise<CardInterfaces | undefined> {
    return await this.cardDB.findOne({ _id: idCard });
  }
  async findByCardNumber(
    cartNumber: string,
  ): Promise<CardInterfaces | undefined> {
    return await this.cardDB.findOne({ cartNumber });
  }

  async findAll(options?: CardTypes): Promise<CardInterfaces[]> {
    return await this.cardDB.find(options).toArray();
  }

  insert(): CardInterfaces {
    throw new Error("Method not implemented.");
  }

  async delete(cardId: string): Promise<number> {
    if (cardId) {
      return await this.cardDB.deleteOne({ _id: cardId });
    }
    return 0;
  }
}
