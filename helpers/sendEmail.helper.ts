import { SmtpClient } from "https://deno.land/x/smtp/mod.ts";
import { config } from "../config/config.ts";

export const SendEmail = async (dest: string) => {
  const client = new SmtpClient();
  await client.connectTLS({
    hostname: "smtp.gmail.com",
    port: 465,
    username: config.GMAIL_USERNAME,
    password: config.GMAIL_PASSWORD,
  });
  await client.send({
    from: "marc.dev.management@gmail.com",
    to: dest,
    subject: "Marc & Adrient IMIE",
    content: "Ceci est un email automatique, merci de ne pas répondre.",
  });

  await client.close();
};
