import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import UserDB from "../db/UserDB.ts";
import UserInterfaces from "../interfaces/userInterfaces.ts";
import { checkToken, getToken } from "../utils/token.ts";

// deno-lint-ignore no-explicit-any
export const authMiddleware = async (ctx: Context, next: any) => {
  try {
    // récupération du token
    const authorization = ctx.request.headers.get("authorization");

    // s'il n'y a pas de header authorization dans la requête
    if (!authorization) {
      ctx.response.status = 401;
      ctx.response.body = {
        "error": true,
        "message": "Votre token n'est pas correct",
      };
      return;
    }

    // vérification du token
    const token = await checkToken(authorization);
    const tokenModels = await getToken(authorization);
    let isTokenValid: boolean;
    if (!tokenModels) {
      isTokenValid = false;
    } else {
      const user: UserInterfaces | undefined = await new UserDB().findByEmail(
        tokenModels.email,
      );
      if (user) {
        isTokenValid = user.token == authorization.split("Bearer ")[1];
      } else {
        isTokenValid = false;
      }
    }
    // si le token est présent mais qu'il n'est pas valide ou si le token est présent mais le user est logout
    if (!token || !isTokenValid) {
      ctx.response.status = 403;
      ctx.response.body = {
        "error": true,
        "message":
          "Vos droits d'accès ne permettent pas d'accéder à la ressource",
      };
      return;
    }
    return await next();
  } catch (error) {
    console.log("error: ", error);
    return await next();
  }
};
