import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import UserDB from "../db/UserDB.ts";
import UserInterfaces from "../interfaces/userInterfaces.ts";
import { roleTypes } from "../types/rolesTypes.ts";
import { checkToken, getToken } from "../utils/token.ts";
import { ChildDB } from "../db/ChildDB.ts";

// deno-lint-ignore no-explicit-any
export const songMiddleware = async (ctx: Context, next: any) => {
  try {
    const cinqMinutes = 300000; // ms
    // récupération du token
    const authorization = ctx.request.headers.get("authorization");

    // s'il n'y a pas de header authorization dans la requête
    if (!authorization) {
      ctx.response.status = 401;
      ctx.response.body = {
        "error": true,
        "message": "Votre token n'est pas correct",
      };
      return;
    }

    // vérification du token
    const token = await getToken(authorization);
    // s'il n'a pas de token mais un header authorization
    if (!token) {
      ctx.response.status = 401;
      ctx.response.body = {
        "error": true,
        "message": "Votre token n'est pas correct",
      };
      return;
    }
    if (token.role as roleTypes === "Enfant") {
      // enfant
      const child = await new UserDB().getChildByEmail(token.email);

      if (!child?.subscription || child?.subscription < 1) {
        ctx.response.status = 403;
        ctx.response.body = {
          "error": true,
          "message": "Votre abonnement ne permet pas d'accéder à la ressource",
        };
        return;
      }
      // période d'essai valide
      if (child?.subscription === 1) {
        const childSubscribeUpdateAt = child.subscribeUpdateAt as Date;
        // si la période d'essai est dépassée
        if (Date.now() - childSubscribeUpdateAt.getTime() > cinqMinutes) {
          ctx.response.status = 403;
          ctx.response.body = {
            "error": true,
            "message":
              "Votre abonnement ne permet pas d'accéder à la ressource",
          };
          return;
        }
      }
    } else {
      // tuteur
      const user = await new UserDB().findByEmail(token.email);
      // si l'utilisateur à un token mais qu'il s'est déconnecté
      // on vérifie avec son token présent en bdd
      if (!user?.token) {
        ctx.response.status = 401;
        ctx.response.body = {
          "error": true,
          "message": "Votre token n'est pas correct",
        };
        return;
      }

      // s'il a un token mais qu'il n'est pas égal à celui en bdd
      if (user.token !== authorization.split("Bearer ")[1]) {
        ctx.response.status = 401;
        ctx.response.body = {
          "error": true,
          "message": "Votre token n'est pas correct",
        };
        return;
      }

      // si le token est présent mais qu'il n'est pas valide ou que son role n'est pas bon
      if (token.role as roleTypes !== "Tuteur") {
        ctx.response.status = 403;
        ctx.response.body = {
          "error": true,
          "message":
            "Vos droits d'accès ne permettent pas d'accéder à la ressource",
        };
        return;
      }

      if (!user?.subscription || user?.subscription < 1) {
        ctx.response.status = 403;
        ctx.response.body = {
          "error": true,
          "message": "Votre abonnement ne permet pas d'accéder à la ressource",
        };
        return;
      }
      // période d'essai valide
      if (user.subscription === 1) {
        const userSubscribeUpdateAt = user.subscribeUpdateAt as Date;

        // si la période d'essai est dépassée
        if (Date.now() - userSubscribeUpdateAt.getTime() > cinqMinutes) {
          ctx.response.status = 403;
          ctx.response.body = {
            "error": true,
            "message":
              "Votre abonnement ne permet pas d'accéder à la ressource",
          };
          return;
        }
      }
    }

    return await next();
  } catch (error) {
    console.log("error: ", error);
    return await next();
  }
};
