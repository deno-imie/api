// deno-lint-ignore-file
import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import UserDB from "../db/UserDB.ts";
import { getToken } from "../utils/token.ts";
import {
  dateValidation,
  emailValidation,
  sexeValidation,
  verifFirstname,
  verifLastname,
  verifPassword,
} from "../helpers/validationUser.helper.ts";
import { IToken } from "../interfaces/tokenInterface.ts";

export const addChildMiddleware = async (
  { request, response }: Context,
  next: any,
) => {
  try {
    const value = await request.body().value;
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;

    const childs = await new UserDB().getChilds(token.email);
    // si le cota d'enfants est dépassé
    if (childs.length === 3) {
      response.status = 409;
      response.body = {
        "error": true,
        "message": "Vous avez dépassé le cota de trois enfants",
      };
      return;
    }

    // vérification des données manquantes
    if (
      !value.email || !value.firstname || !value.lastname || !value.password ||
      !value.sexe || !value.date_naissance
    ) {
      response.status = 400;
      response.body = {
        "error": true,
        "message": "Une ou plusieurs données obligatoire sont manquantes",
      };
      return;
    }

    // vérification de la conformité des données
    if (
      !emailValidation(value.email) ||
      !dateValidation(value.date_naissance) ||
      !sexeValidation(value.sexe) ||
      !verifFirstname(value.firstname) || !verifLastname(value.lastname) ||
      !verifPassword(value.password)
    ) {
      response.status = 409;
      response.body = {
        "error": false,
        "message": "Une ou plusieurs données sont erronées",
      };
      return;
    }

    // vérification si l'email existe déjà pour les enfants
    const childEmail = await new UserDB().getChildByEmail(value.email);
    if (childEmail) {
      response.status = 409;
      response.body = {
        "error": true,
        "message": "Un compte utilisant cette adresse mail est déjà enregistré",
      };
      return;
    }

    // vérification si l'email existe déjà pour un tuteur
    const childEmail2 = await new UserDB().findByEmail(value.email);
    if (childEmail2) {
      response.status = 409;
      response.body = {
        "error": true,
        "message": "Un compte utilisant cette adresse mail est déjà enregistré",
      };
      return;
    }

    return await next();
  } catch (error) {
    console.log("error: ", error);
    return await next();
  }
};
