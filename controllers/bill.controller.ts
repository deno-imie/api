import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import UserDB from "../db/UserDB.ts";
import BillInterface from "../interfaces/BillInterface.ts";
import { IToken } from "../interfaces/tokenInterface.ts";
import { getToken } from "../utils/token.ts";
import { formatDatePayment } from "../helpers/validationUser.helper.ts";

/**
 * GET BILL
 */
export const getBills = async ({ request, response }: Context) => {
  const authorization = await request.headers.get("authorization") as string;
  const token = await getToken(authorization) as IToken;

  const user = await new UserDB().findByEmail(token.email);
  var bills = [];
  if (user) {
    if (user.bills) {
      for (const bill of user.bills) {
        bills.push({
          "id": bill._id,
          "id_Stripe": bill.id_Stripe,
          "date_payment": formatDatePayment(bill.date_payment),
          "montant_ht": bill.montant_ht,
          "montant_ttc": bill.montant_ttc,
          "source": "Stripe",
          "createdAt": bill.createdAt,
          "updateAt": bill.updateAt,
        });
      }
    }
  }
  response.status = 200;
  response.body = {
    "error": false,
    "bill": bills,
  };
};
