// deno-lint-ignore-file
import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";

export const index = async ({ request, response }: Context) => {
  response.status = 200;
  response.type = "text/html; charset=utf-8";
  response.body = `<H1>Hello</H1>`;
  return;
};
