// deno-lint-ignore-file
import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import { getQuery } from "https://deno.land/x/oak/helpers.ts";
import { play } from "https://deno.land/x/audio@0.1.0/mod.ts";

/**
 * LIST SONGS
 */
export const listSongs = async (ctx: Context) => {
  const songs = [{
    "id": "1",
    "name": "1",
    "url": "http://localhost:3000/songs/1",
    "cover": "1.jpg",
    "time": "00:02:03",
    "type": "mp3",
    "createdAt": "2020-12-29T18:01:57.181+00:00",
    "updateAt": "2020-12-29T18:01:57.181+00:00",
  }, {
    "id": "2",
    "name": "2",
    "url": "http://localhost:3000/songs/2",
    "cover": "2.jpg",
    "time": "00:02:04",
    "type": "mp3",
    "createdAt": "2021-01-28T21:04:05.129+00:00",
    "updateAt": "2021-01-28T21:04:05.129+00:00",
  }];

  ctx.response.status = 200;
  ctx.response.body = {
    "error": false,
    songs,
  };
  return;
};

/**
  * GET AUDIO SONG
  */
export const getSong = async (ctx: Context) => {
  const params = getQuery(ctx, { mergeParams: true });

  let song = {};

  switch (params.id) {
    case "1":
      play("1.mp3");
      song = {
        "id": params.id,
        "name": params.id,
        "url": "http://localhost:3000/songs/" + params.id,
        "cover": params.id + ".jpg",
        "time": "00:02:03",
        "type": "mp3",
        "createdAt": "2020-12-29T18:01:57.181+00:00",
        "updateAt": "2020-12-29T18:01:57.181+00:00",
      };
      break;
    case "2":
      play("2.mp3");
      song = {
        "id": params.id,
        "name": params.id,
        "url": "http://localhost:3000/songs/" + params.id,
        "cover": params.id + ".jpg",
        "time": "00:02:04",
        "type": "mp3",
        "createdAt": "2021-01-28T21:04:05.129+00:00",
        "updateAt": "2021-01-28T21:04:05.129+00:00",
      };
      break;
    default:
      play("1.mp3");
      song = {
        "id": params.id,
        "name": params.id,
        "url": "http://localhost:3000/songs/" + params.id,
        "cover": params.id + ".jpg",
        "time": "00:02:03",
        "type": "mp3",
        "createdAt": new Date(),
        "updateAt": new Date(),
      };
      break;
  }

  ctx.response.status = 201;
  ctx.response.body = {
    "error": false,
    song,
  };
  return;
};
