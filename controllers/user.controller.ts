// deno-lint-ignore-file
import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import UserDB from "../db/UserDB.ts";
import CardDB from "../db/CardDB.ts";
import { UserModels } from "../models/UserModels.ts";
import { getToken } from "../utils/token.ts";
import { ChildModels } from "../models/ChildModel.ts";
import {
  dateNaissanceValidation,
  emailValidation,
  formatDateNaissance,
  sexeValidation,
  verifCartNumber,
  verifDate,
  verifFirstname,
  verifLastname,
  verifMonth,
  verifPassword,
  verifYear,
} from "../helpers/validationUser.helper.ts";
import { CardModels } from "../models/CardModels.ts";
import { verifCvc, verifIdCart } from "../helpers/validationUser.helper.ts";
import { IToken } from "../interfaces/tokenInterface.ts";
import UserInterfaces from "../interfaces/userInterfaces.ts";
import { BillModel } from "../models/BillModel.ts";
import { stripeRequest } from "../utils/stripeRequest.ts";
import CardInterfaces from "../interfaces/cardInterfaces.ts";
import { SendEmail } from "../helpers/sendEmail.helper.ts";
/**
 * EDIT USER  
 */

export const editUser = async ({ request, response }: Context) => {
  try {
    let update = await request.body().value;
    //recupération et verfication du token
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;

    //verification des props accepté dans l'update
    for (var key in update) {
      if (
        key !== "firstname" && key !== "lastname" && key !== "date_naissance" &&
        key !== "sexe"
      ) {
        response.status = 409;
        response.body = {
          "error": true,
          "message": "Une ou plusieurs données sont erronées",
        };
        return;
      }
    }
    //verification du format des values de l'update
    if (
      (update.dateNaissance && !dateNaissanceValidation(update.dateNaissance) &&
        !formatDateNaissance(update.dateNaissance)) ||
      (update.sexe && !sexeValidation(update.sexe)) ||
      (update.firstname && !verifFirstname(update.firstname)) ||
      (update.lastname && !verifLastname(update.lastname))
    ) {
      response.status = 409;
      response.body = {
        "error": true,
        "message": "Une ou plusieurs données sont erronées",
      };
      return;
    }
    //recuperation des data du user
    const user = await new UserDB().findByEmail(token.email);
    if (user) {
      user.firstname = update.firstname ? update.firstname : user.firstname;
      user.lastname = update.lastname ? update.lastname : user.lastname;
      user.date_naissance = update.dateNaissance
        ? update.dateNaissance
        : user.date_naissance;
      user.sexe = update.sexe ? update.sexe : user.sexe;
      //udpate en fonction des data reçus
      await new UserDB().updateOne(user.email, user);
    }
    response.status = 200;
    response.body = {
      "error": false,
      "message": "Vos données ont été mises à jour",
    };
  } catch (error) {
    console.log("error: ", error);
  }
};

/**
  * LOGOUT
  */
export const logout = async ({ request, response }: Context) => {
  try {
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization);
    // éviter erreur TS avec le token qui peut être égal à false
    if (!token) {
      response.status = 401;
      response.body = {
        "error": true,
        "message": "Votre token n'est pas correct",
      };
      return;
    }
    let user = await new UserDB().findByEmail(token.email);
    if (user) {
      user.token = "";
      await new UserDB().updateOne(token.email, user);
      response.status = 200;
      response.body = {
        "error": false,
        "message": "L'utilisateur a été déconnecté avec succès",
      };
    }
  } catch (error) {
    console.log("error: ", error);
  }
};
/**
   * ADD CHILD
   */
export const addChild = async ({ request, response }: Context) => {
  try {
    const value = await request.body().value;
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;
    const user_ = await new UserDB().findByEmail(token.email) as UserInterfaces;
    const child = await new ChildModels(
      value.firstname,
      value.lastname,
      value.email,
      value.sexe,
      value.password,
      value.date_naissance,
      user_.subscription,
    ).generate();

    const user = await new UserDB().addChild(token.email, child);
    // récupération de l'enfant ajouté
    const childAdded = user.childs?.find((v) => v.email === value.email);
    let data = {
      firstname: childAdded?.firstname,
      lastname: childAdded?.lastname,
      sexe: childAdded?.sexe,
      role: childAdded?.role,
      dateNaissance: formatDateNaissance(childAdded?.date_naissance as Date),
      createdAt: childAdded?.createdAt,
      updateAt: childAdded?.updateAt,
      subscription: childAdded?.subscription, //TODO: demander si y'a subscription dans l'enfant
    };

    response.status = 201;
    response.body = {
      "error": false,
      "message": "Votre enfant a bien été créé avec succès",
      "user": data,
    };
  } catch (error) {
    console.log("error: ", error);
  }
};
/**
    * DELETE CHILD
    */
export const deleteChild = async ({ request, response }: Context) => {
  try {
    const body = await request.body().value;
    const _id = body.id_child;
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;

    // suppression de l'enfant
    const isDeleted = await new UserDB().deleteChild(token.email, _id);

    // si l'id n'est pas bon
    if (!isDeleted) {
      response.status = 403;
      response.body = {
        "error": true,
        "message": "Vous ne pouvez pas supprimer cet enfant",
      };
      return;
    }

    response.status = 200;
    response.body = {
      "error": false,
      "message": "L'utilisateur a été supprimée avec succès",
    };
  } catch (error) {
    console.log("error: ", error);
  }
};

/**
     * LIST CHILD
     */
export const listChilds = async ({ request, response }: Context) => {
  try {
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;

    // récup des enfants
    const childs = await new UserDB().getChilds(token.email);
    const data: any[] = [];
    childs.forEach((child) => {
      data.push({
        firstname: child.firstname,
        lastname: child.lastname,
        sexe: child.sexe,
        role: child.role,
        dateNaissance: formatDateNaissance(child.date_naissance as Date),
        createdAt: child.createdAt,
        updateAt: child.updateAt,
        subscription: child.subscription,
      });
    });

    response.status = 200;
    response.body = {
      "error": false,
      "users": data,
    };
  } catch (error) {
    console.log("error: ", error);
  }
};

/**
      * ADD BANK CARD
      */
export const addCard = async ({ request, response }: Context) => {
  try {
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;
    const value = await request.body().value;
    // éviter erreur TS avec le token qui peut être égal à false

    if (
      !value.cartNumber ||
      !value.month ||
      !value.year ||
      !value.default
    ) {
      response.status = 403;
      response.body = {
        "error": true,
        "message": "Veuillez compléter votre profil avec une carte de crédit",
      };
      return;
    }

    if (
      !verifCartNumber(value.cartNumber) ||
      !verifMonth(value.month) ||
      !verifYear(value.year)
    ) {
      response.status = 402;
      response.body = {
        "error": true,
        "message": "Informations bancaire incorrectes",
      };
      return;
    }

    for (var item in value) {
      if (
        item !== "cartNumber" && item !== "month" && item !== "year" &&
        item !== "default"
      ) {
        response.status = 409;
        response.body = {
          "error": true,
          "message": "Une ou plusieurs données sont erronées",
        };
        return;
      }
    }

    const user = await new UserDB().findByEmail(token.email);

    if (user?.card?.cartNumber === String(value.cartNumber)) {
      response.status = 409;
      response.body = {
        "error": true,
        "message": "La carte existe déjà",
      };
      return;
    }

    if (user) {
      var newCard = new CardModels(
        value.cartNumber,
        value.month,
        value.year,
        value.default,
      ).insert();
      user.card = newCard;
      await new UserDB().updateOne(token.email, user);
      response.status = 200;
      response.body = {
        "error": false,
        "message": "Vos données ont été mises à jour",
      };
    }
  } catch (error) {
    console.log("error: ", error);
  }
};

/**
       * SUBSCRIBE
       */
export const subscribe = async ({ request, response }: Context) => {
  try {
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;
    const body: { idCard: number; cvc: number } = await request.body()
      .value;

    // vérification des données envoyées (idcarte/cvc)
    if (!body.cvc || !body.idCard) {
      response.status = 400;
      response.body = {
        "error": true,
        "message": "Une ou plusieurs données obligatoire sont manquantes",
      };
      return;
    }

    // récupération du profil
    const user = await new UserDB().findByEmail(token.email) as UserInterfaces;
    // vérification de la conformité des données envoyées (idcarte/cvc)
    if (
      user.card?._id?.toString() !== String(body.idCard) ||
      !verifCvc(String(body.cvc))
    ) {
      response.status = 402;
      response.body = {
        "error": true,
        "message": "Echec du payement de l'offre",
      };
      return;
    }

    const childs = user.childs || [];
    // si l'utilisateur n'a pas d'abonnement
    // on active sa période d'essai
    if (user.subscription === 0) {
      // mis à jour de l'abonnement de l'enfant
      childs?.map((child) => {
        child.subscription = 1;
        child.subscribeUpdateAt = new Date();
      });
      // mise à jour de user.subscription à 1
      await new UserModels(
        user.firstname,
        user.lastname,
        user.email,
        user.sexe,
        user.password,
        user.date_naissance,
      ).update(
        {
          subscription: 1,
          subscribeCreatedAt: new Date(),
          subscribeUpdateAt: new Date(),
          childs,
          // cardId: body.idCard.toString(),
          cvc: body.cvc,
        },
      );
      //TODO: envoi du mail de souscription
      response.status = 200;
      response.body = {
        "error": false,
        "message": "Votre période d'essai viens d'être activé - 5min",
      };
      return;
    }

    // abonnement déjà activé donc fin de période d'essai
    // mis à jour de l'abonnement de l'enfant
    childs?.map((child) => {
      child.subscription = 2;
      child.subscribeUpdateAt = new Date();
    });

    // ######################
    //        STRIPE
    // ######################
    // requete stripe
    const sub = await stripeRequest(
      token.email,
      user.card?.cartNumber as string,
      user.card?.month as string,
      user.card?.year as string,
      String(body.cvc),
    );

    // erreur paiement stripe
    if (sub?.error) {
      response.status = 202;
      response.body = {
        "error": true,
        "message": "Echec du payement de l'offre",
      };
      return;
    }

    const bill = await new BillModel(
      sub.id,
      String((sub.items.data[0].plan.amount / 100).toFixed(2)),
      String((sub.items.data[0].plan.amount / 100).toFixed(2)),
    ).generate();
    // ajout de la facture dans le table user
    const { bills } = await new UserDB().addBill(token.email, bill);
    // maj de l'abonnement
    await new UserModels(
      user.firstname,
      user.lastname,
      user.email,
      user.sexe,
      user.password,
      user.date_naissance,
    ).update(
      {
        subscription: 2,
        subscribeUpdateAt: new Date(),
        // cardId: body.idCard.toString(),
        cvc: body.cvc,
        childs,
        bills,
      },
    );

    //TODO: envoie du mail d'abonnement
    await SendEmail(user.email);

    response.status = 200;
    response.body = {
      "error": false,
      "message": "Votre abonnement a bien été mise à jour",
    };
    return;
  } catch (error) {
    console.log("error: ", error);
  }
};

/**
       * REMOVE ACCOUNT
       */
export const removeAccount = async ({ request, response }: Context) => {
  try {
    const authorization = await request.headers.get("authorization") as string;
    const token = await getToken(authorization) as IToken;
    // éviter erreur TS avec le token qui peut être égal à false
    if (!token) {
      response.status = 401;
      response.body = {
        "error": true,
        "message": "Votre token n'est pas correct",
      };
      return;
    }

    const user = await new UserDB().findByEmail(token.email);
    // si l'utilisateur est un enfant, il ne peut pas supprimer d'autres enfants
    if (user?.role !== "Tuteur") {
      response.status = 403;
      response.body = {
        "error": true,
        "message":
          "Vos droits d'accès ne permettent pas d'accéder à la ressource",
      };
      return;
    }

    new UserDB().delete(token.email);
    response.status = 200;
    response.body = {
      "error": false,
      "message":
        "Votre compte et le compte de vos enfants ont été supprimés avec succès",
    };
  } catch (error) {
    console.log("error: ", error);
  }
};
