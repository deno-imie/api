// deno-lint-ignore-file
import { Context } from "https://deno.land/x/oak@v6.4.1/context.ts";
import {
  dateNaissanceValidation,
  dateValidation,
  emailValidation,
  formatDateNaissance,
  sexeValidation,
  verifDate,
  verifFirstname,
  verifLastname,
  verifPassword,
} from "../helpers/validationUser.helper.ts";
import { UserModels } from "../models/UserModels.ts";
import { comparePass } from "../helpers/password.helper.ts";
import { SendEmail } from "../helpers/sendEmail.helper.ts";
import { createToken } from "../utils/token.ts";
import { ChildDB } from "../db/ChildDB.ts";
import UserInterfaces from "../interfaces/userInterfaces.ts";
import UserDB from "../db/UserDB.ts";

/**
 * REGISTER
 */
export const register = async ({ request, response }: Context) => {
  try {
    var resUser: UserModels;
    // var user: UserModels;
    resUser = await request.body().value;
    if (
      !resUser.firstname ||
      !resUser.lastname ||
      !resUser.email ||
      !resUser.date_naissance ||
      !resUser.sexe ||
      !resUser.password
    ) {
      response.status = 400;
      response.body = {
        "error": false,
        "message": "Une ou plusieurs données obligatoire sont manquantes",
      };
      return;
    }
    if (
      !sexeValidation(resUser.sexe) ||
      !verifFirstname(resUser.firstname) ||
      !verifLastname(resUser.lastname) ||
      !verifPassword(resUser.password) ||
      !emailValidation(resUser.email) ||
      !dateNaissanceValidation(new Date(resUser.date_naissance)) ||
      !dateValidation(resUser.date_naissance.toString())
    ) {
      console.log(
        !sexeValidation(resUser.sexe),
        !verifFirstname(resUser.firstname),
        !verifLastname(resUser.lastname),
        !verifPassword(resUser.password),
        !emailValidation(resUser.email),
        !dateNaissanceValidation(new Date(resUser.date_naissance)),
        !dateValidation(resUser.date_naissance.toString()),
      );
      response.status = 409;
      response.body = {
        "error": false,
        "message": "Une ou plusieurs données sont erronées",
      };
      return;
    }

    var userFromDb = await new UserDB().findByEmail(resUser.email);

    if (userFromDb) {
      response.status = 409;
      response.body = {
        "error": true,
        "message": "Un compte utilisant cette adresse mail est déjà enregistré",
      };
      return;
    }

    for (const props in resUser) {
      if (
        props !== "firstname" &&
        props !== "lastname" &&
        props !== "email" &&
        props !== "sexe" &&
        props !== "password" &&
        props !== "date_naissance"
      ) {
        response.status = 409;
        response.body = {
          "error": false,
          "message": "Une ou plusieurs données sont erronées",
        };
        return;
      }
    }

    await new UserModels(
      resUser.firstname,
      resUser.lastname,
      resUser.email,
      resUser.sexe,
      resUser.password,
      resUser.date_naissance,
    ).insert();

    SendEmail(resUser.email);

    const user = await new UserDB().findByEmail(
      resUser.email,
    ) as UserInterfaces;

    let data = {
      firstname: user.firstname,
      lastname: user.lastname,
      sexe: user.sexe,
      role: user.role,
      dateNaissance: formatDateNaissance(user.date_naissance as Date),
      createdAt: user.createdAt,
      updateAt: user.updateAt,
      subscription: user.subscription,
    };
    response.status = 201;
    response.body = {
      "error": false,
      "message": "L'utilisateur a bien été créé avec succès",
      "user": data,
    };
  } catch (error) {
    console.log("error: ", error);
  }
};

/**
  * LOGIN
  */
// deno-lint-ignore no-explicit-any
const numberTentatives: any = {}; // ajout dynamiquement des nombres de tentatives
export const login = async ({ request, response }: Context) => {
  try {
    const ip = await request.ip;

    const { email, password }: { email: string; password: string } =
      await request
        .body().value;

    // si l'email et le mdp ne sont pas renseignés
    if (!email || !password) {
      response.status = 400;
      response.body = { "error": true, "message": "Email/password manquants" };
      return;
    }

    // ##########################################################################
    // ########################  TEST TENTATIVES  ###############################
    // ##########################################################################
    // récupération de l'ip qu'on stocke avec l'email visé pour se connecter
    const tentativeIp = numberTentatives[ip];
    // si l'ip est stocké
    if (tentativeIp) {
      // on récupère l'email visé
      const tentativeEmail = tentativeIp[email];
      // si l'email existe avec l'ip
      if (tentativeEmail) {
        // si expire est renseigné (donc 5 tentatives échouées)
        if (tentativeEmail?.expire) {
          // si les 2min ne sont pas écoulées
          if (tentativeEmail.expire as number - Date.now() > 0) {
            // remise à 0 de la tentative pour la personne quand son temps sera écoulé
            tentativeEmail.tentative = 0;
            // réponse serveur
            response.status = 429;
            response.body = {
              "error": true,
              "message":
                `Trop de tentative sur l'email ${email} (5 max) - Veuillez patienter (2min)`,
            };
            return;
          }
        }
        // on récupère son nombre de tentative pour l'incrémenter
        const tentative = tentativeEmail?.tentative + 1;
        let expire = null;
        // si le nombre de tentative est égal à une certaine valeur
        if (tentative === 5) {
          //TODO: modifier les 10000 -> 120000 pour la version final
          expire = Date.now() + 120000; // date + 2min (10s pour les tests en attendant)
        }
        // ajout des valeurs + les anciennes
        numberTentatives[ip] = {
          ...numberTentatives[ip],
          [email]: {
            tentative: tentative,
            expire,
          },
        };
      } else {
        // sinon on initialise les valeurs en ajoutant les anciennes dynamiquement s'il y'en a
        numberTentatives[ip] = {
          ...numberTentatives[ip],
          [email]: {
            tentative: 1,
            expire: null,
          },
        };
      }
    } else {
      // sinon on initialise les valeurs en ajoutant les anciennes dynamiquement s'il y'en a
      numberTentatives[ip] = {
        ...numberTentatives[ip],
        [email]: {
          tentative: 1,
          expire: null,
        },
      };
    }
    // ##########################################################################
    // ######################  FIN TEST TENTATIVES  #############################
    // ##########################################################################
    let user;
    // admin
    user = await new UserDB().findByEmail(email);
    if (!user) {
      // on teste pour voir si c'est un enfant
      user = await new UserDB().getChildByEmail(email);
    }
    // mauvais email
    if (!user) {
      response.status = 400;
      response.body = { "error": true, "message": "Email/password incorrect" };
      return;
    }

    const goodPassword = await comparePass(password, user.password);
    // mauvais mdp
    if (!goodPassword) {
      response.status = 400;
      response.body = { "error": true, "message": "Email/password incorrect" };
      return;
    }
    // suppression de son nombres de tentatives si le mdp est bon
    delete numberTentatives[ip];
    // création token
    const token = await createToken(user);

    await new UserModels(
      user.firstname,
      user.lastname,
      user.email,
      user.sexe,
      user.password,
      user.date_naissance,
    ).update({ token });

    const data = {
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      sexe: user.sexe,
      role: user.role,
      dateNaissance: formatDateNaissance(user.date_naissance as Date),
      createdAt: user.createdAt,
      updateAt: user.updateAt,
      subscription: user.subscription,
    };

    response.status = 200;
    response.body = {
      "error": false,
      "message": "L'utilisateur a été authentifié succès",
      "user": data,
      "token": token,
    };
    return;
  } catch (error) {
    console.log("error: ", error);
  }
};
