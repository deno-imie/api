// deno-lint-ignore-file
import UserDB from "../db/UserDB.ts";
import { hash } from "../helpers/password.helper.ts";
import CardInterfaces from "../interfaces/cardInterfaces.ts";
import UserInterfaces from "../interfaces/userInterfaces.ts";
import { roleTypes } from "../types/rolesTypes.ts";
import { sexeTypes } from "../types/sexeTypes.ts";
import { UserTypes } from "../types/userTypes.ts";

export class UserModels extends UserDB implements UserInterfaces {
  private _role: roleTypes = "Tuteur";
  private id: { $oid: string } | null = null;

  firstname: string;
  lastname: string;
  email: string;
  sexe: sexeTypes;
  role: roleTypes;
  password: string;
  date_naissance: Date;
  createdAt: Date;
  updateAt: Date;
  subscription: number;
  card?: CardInterfaces;
  subscribeCreatedAt?: Date | null;
  subscribeUpdateAt?: Date | null;
  token?: string;

  constructor(
    prenom: string,
    nom: string,
    email: string,
    sexe: sexeTypes,
    password: string,
    dateNaissance: Date,
  ) {
    super();
    this.firstname = prenom;
    this.lastname = nom;
    this.email = email;
    this.sexe = sexe;
    this.role = "Tuteur";
    this.password = password;
    this.date_naissance = new Date(dateNaissance);
    this.createdAt = new Date();
    this.updateAt = new Date();
    this.subscription = 0;
    this.token = "";
  }

  get _id(): string | null {
    return (this.id === null) ? null : this.id.$oid;
  }

  get getRole(): roleTypes {
    return this._role;
  }
  setRole(role: roleTypes): void {
    this._role = role;
    this.update({ role: role });
  }
  getAge(): number {
    var ageDifMs = Date.now() - this.date_naissance.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }
  fullName(): string {
    return "${this.lastname} ${this.firstname}";
  }

  async insert(): Promise<void> {
    try {
      this.password = await hash(this.password);
      this.id = await this.userdb.insertOne({
        firstname: this.firstname,
        lastname: this.lastname,
        email: this.email,
        sexe: this.sexe,
        role: this.role,
        password: this.password,
        date_naissance: this.date_naissance,
        createdAt: this.createdAt,
        updateAt: this.updateAt,
        subscription: this.subscription,
        token: this.token,
      }) as {
        $oid: string;
      };
    } catch (error) {
      console.log("error: ", error);
    }
  }

  async update(update: UserTypes): Promise<void> {
    try {
      // updateOne
      const { modifiedCount } = await this.userdb.updateOne(
        { email: this.email },
        { $set: update },
      );
    } catch (error) {
      console.log("error: ", error);
    }
  }

  async delete(email: string): Promise<any> {
    try {
      if (email) {
        return await this.userdb.deleteOne(this.findByEmail(email));
      } else {
        return 0;
      }
    } catch (error) {
      console.log("error: ", error);
    }
  }
}
