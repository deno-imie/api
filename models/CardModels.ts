// deno-lint-ignore-file
import CardDB from "../db/CardDB.ts";
import CardInterfaces from "../interfaces/cardInterfaces.ts";
import { Bson } from "https://deno.land/x/bson/mod.ts";

export class CardModels extends CardDB implements CardInterfaces {
  private id: { $oid: string } | null = null;

  cartNumber: string;
  month: string;
  year: string;
  defaultCard: boolean;
  cvc?: number | undefined;

  constructor(
    cartNumber: string,
    month: string,
    year: string,
    defaultCard: boolean,
  ) {
    super();
    this.cartNumber = cartNumber;
    this.month = month;
    this.year = year;
    this.defaultCard = defaultCard;
  }

  get _id(): string | null {
    return (this.id === null) ? null : this.id.$oid;
  }

  getCardNumber(): string {
    return this.cartNumber;
  }
  getMonth(): string {
    return this.month;
  }
  getYear(): string {
    return this.year;
  }
  getDefault(): boolean {
    return this.defaultCard;
  }

  // deno-lint-ignore no-explicit-any
  insert(): any {
    return {
      _id: new Bson.ObjectId(),
      cartNumber: this.cartNumber,
      month: this.month,
      year: this.year,
      defaultCard: this.defaultCard,
    };
  }

  async delete(idCard: string): Promise<any> {
    try {
      if (idCard) {
        return await this.cardDB.deleteOne(this.findByCardIdCard(idCard));
      } else {
        return 0;
      }
    } catch (error) {
      console.log("error: ", error);
    }
  }
}
