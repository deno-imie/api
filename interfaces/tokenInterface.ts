import { roleTypes } from "../types/rolesTypes.ts";

export interface IToken {
  sub: string | undefined;
  email: string;
  firstname: string;
  lastname: string;
  role: roleTypes;
  exp: number | undefined;
}
