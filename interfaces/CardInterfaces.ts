export default interface CardInterfaces {
  _id: { $oid: string } | null | string;
  cartNumber: string;
  month: string;
  year: string;
  cvc?: number;
  defaultCard: boolean;

  insert(): CardInterfaces;
  delete(idCard: string): Promise<number>;
}
