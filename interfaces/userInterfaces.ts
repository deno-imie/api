import { roleTypes } from "../types/rolesTypes.ts";
import { sexeTypes } from "../types/sexeTypes.ts";
import { UserTypes } from "../types/userTypes.ts";
import BillInterface from "./BillInterface.ts";
import CardInterfaces from "./cardInterfaces.ts";
import ChildInterface from "./ChildInterfaces.ts";

export default interface UserInterfaces {
  _id: { $oid: string } | null | string;

  firstname: string;
  lastname: string;
  email: string;
  sexe: sexeTypes;
  role: roleTypes;
  password: string;
  date_naissance: Date;
  createdAt: Date;
  updateAt: Date;
  subscription: number;
  childs?: ChildInterface[];
  bills?: BillInterface[];
  connection?: boolean;
  token?: string;
  card?: CardInterfaces;
  subscribeCreatedAt?: Date | null;
  subscribeUpdateAt?: Date | null;

  getAge(): number;
  fullName(): string;
  insert(): Promise<void>;
  update(update: UserTypes): Promise<void>;
  delete(email: string): Promise<number>;
}
