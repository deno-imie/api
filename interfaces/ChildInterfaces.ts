import { roleTypes } from "../types/rolesTypes.ts";
import { sexeTypes } from "../types/sexeTypes.ts";
import { UserTypes } from "../types/userTypes.ts";

export default interface ChildInterface {
  _id: { $oid: string } | null | string;
  firstname: string;
  lastname: string;
  email: string;
  sexe: sexeTypes;
  role: roleTypes;
  password: string;
  date_naissance: Date;
  createdAt: Date;
  updateAt: Date;
  token?: string;
  subscription: number;
  subscribeCreatedAt?: Date | null;
  subscribeUpdateAt?: Date | null;

  getAge(): number;
  fullName(): string;
  insert(): Promise<void>;
  update(update: UserTypes): Promise<void>;
  delete(): Promise<void>;
}
