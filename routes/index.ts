import { Router } from "https://deno.land/x/oak/mod.ts";
import { login, register } from "../controllers/auth.controller.ts";
import { getSong, listSongs } from "../controllers/songs.controller.ts";
import {
  addCard,
  addChild,
  deleteChild,
  editUser,
  listChilds,
  logout,
  removeAccount,
  subscribe,
} from "../controllers/user.controller.ts";
import { tuteurRoleMiddleware } from "../middlewares/tuteurRole.middleware.ts";
import { addChildMiddleware } from "../middlewares/child.middleware.ts";
import { songMiddleware } from "../middlewares/song.middleware.ts";
import { authMiddleware } from "../middlewares/auth.middleware.ts";
import { getBills } from "../controllers/bill.controller.ts";
import { index } from "../controllers/index.controller.ts";
const router = new Router();

// Login
router.post("/login", login);
router.post("/register", register);

// user
router.put("/user", authMiddleware, editUser);
router.delete("/user", tuteurRoleMiddleware, removeAccount);
router.delete("/user/off", authMiddleware, logout);
router.put("/user/cart", tuteurRoleMiddleware, addCard);

// user/child
router.get("/user/child", tuteurRoleMiddleware, listChilds);
router.post("/user/child", tuteurRoleMiddleware, addChildMiddleware, addChild);
router.put("/user/subscription", tuteurRoleMiddleware, subscribe);
router.delete("/user/child", tuteurRoleMiddleware, deleteChild);

// song
router.get("/songs", songMiddleware, listSongs);
router.get("/songs/:id", songMiddleware, getSong);

// bills
router.get("/bills", tuteurRoleMiddleware, getBills);

// index
router.get("/", index);

export default router;
