// deno-lint-ignore-file
import UserDB from "../db/UserDB.ts";
import { UserModels } from "../models/UserModels.ts";
import { everyMinute } from "https://deno.land/x/deno_cron/cron.ts";
import { BillModel } from "../models/BillModel.ts";
import { stripeRequest } from "../utils/stripeRequest.ts";
import { SendEmail } from "../helpers/sendEmail.helper.ts";

everyMinute(async () => {
  console.log("cron");
  const cinqMinutes = 300000; // ms
  try {
    const users = await new UserDB().findAll();
    for await (const user of users) {
      // si la période d'essai a été activée

      if (user.subscription === 1) {
        //vérification de la période d'essai
        // user.subscribeCreatedAt existe quand on met à jour subscription à 1
        const subscribeCreatedAt = user.subscribeCreatedAt as Date;
        // si la période d'essai a exipré
        if (
          Date.now() - new Date(subscribeCreatedAt).getTime() >= cinqMinutes
        ) {
          const childs = user.childs || [];
          // mis à jour de l'abonnement de l'enfant
          childs?.map((child) => {
            child.subscription = 2;
            child.subscribeUpdateAt = new Date();
          });
          // abonnement stripe
          const sub = await stripeRequest(
            user.email,
            user.card?.cartNumber as string,
            user.card?.month as string,
            user.card?.year as string,
            String(user.card?.cvc),
          );
          console.log(
            "stripe facture fin de période d'essai, début de la payante",
          );
          // ajout de la facture
          const prix = isNaN(sub.items?.data[0].plan.amount)
            ? "5.00"
            : (sub.items?.data[0].plan.amount / 100).toFixed(2);
          const bill = await new BillModel(sub.id, prix, prix).generate();
          // ajout de la facture dans le table user
          const { bills } = await new UserDB().addBill(user.email, bill);

          // maj de l'abonnement
          await new UserModels(
            user.firstname,
            user.lastname,
            user.email,
            user.sexe,
            user.password,
            user.date_naissance,
          ).update({
            subscribeCreatedAt: new Date(),
            subscribeUpdateAt: new Date(),
            subscription: 2,
            childs,
            bills,
          });

          //TODO: envoi d'un mail à l'utilisateur pour lui annoncer la fin de son abonnement d'essai
          SendEmail(user.email);
        }
      }
      // si l'abonnement est activé, on envoie un mail
      if (user.subscription === 2) {
        // récupération des factures
        let bills = user.bills || [];
        const oldBillsLength = bills.length;
        // user.subscribeUpdateAt existe quand on met à jour subscription à 1
        const subscribeUpdateAt = user.subscribeUpdateAt as Date;
        if (Date.now() - new Date(subscribeUpdateAt).getTime() >= cinqMinutes) {
          // vérification de la dernière facture si elle est supérieure à 5min
          if (bills[oldBillsLength - 1]?.date_payment) {
            if (
              Date.now() -
                  new Date(bills[oldBillsLength - 1].date_payment).getTime() >=
                cinqMinutes
            ) {
              // abonnement stripe
              const sub = await stripeRequest(
                user.email,
                user.card?.cartNumber as string,
                user.card?.month as string,
                user.card?.year as string,
                String(user.card?.cvc),
              );
              // ajout de la facture
              const prix = isNaN(sub.items?.data[0].plan.amount)
                ? "5.00"
                : (sub.items?.data[0].plan.amount / 100).toFixed(2);
              // ajout de la facture
              const bill = await new BillModel(sub.id, prix, prix).generate();
              // ajout de la facture dans le table user
              const data = await new UserDB().addBill(user.email, bill);
              bills = data.bills || [];
              console.log("stripe facture auto");
            }
          }
          // maj de la date de l'abonnement
          await new UserModels(
            user.firstname,
            user.lastname,
            user.email,
            user.sexe,
            user.password,
            user.date_naissance,
          ).update({
            subscribeUpdateAt: new Date(),
            bills,
          });

          //TODO: envoi d'un mail à l'utilisateur pour lui envoyer sa facture
        }
      }
    }
  } catch (error) {
    console.log("cron error: ", error);
  }
});
